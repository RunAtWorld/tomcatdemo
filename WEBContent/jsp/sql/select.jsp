<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<html>
<head>
    <title>SELECT 操作</title>
</head>
<body>
<c:out value="sql标签不能识别 maven的包，只能自己引入包到lib目录下"></c:out>
<!--
JDBC 驱动名及数据库 URL
数据库的用户名与密码，需要根据自己的设置
useUnicode=true&characterEncoding=utf-8 防止中文乱码
 -->
<sql:setDataSource var="mydb2" driver="com.mysql.cj.jdbc.Driver"
                   url="jdbc:mysql://localhost:3306/mydb2?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai"
                   user="root"  password="123456"/>
<h1>JSP 数据库实例 SELECT 操作</h1>
<sql:query dataSource="${mydb2}" var="result">
    SELECT * from websites;
</sql:query>
<table border="1" width="100%">
    <tr>
        <th>ID</th>
        <th>站点名</th>
        <th>站点地址</th>
    </tr>
    <c:forEach var="row" items="${result.rows}">
        <tr>
            <td><c:out value="${row.id}"/></td>
            <td><c:out value="${row.name}"/></td>
            <td><c:out value="${row.url}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
