<jsp:text><![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">]]></jsp:text>
<%@page import="java.util.Calendar" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="dev.demo.jsp.Person" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<html>
<head>
    <title>tomcat demo</title>
</head>
<body>

<hr/>
执行脚本测试信息：
<br/>

<%
    Person person = new Person();
    System.out.print(person.getPersonInfo());
%>


<%= person.toString()%>
<jsp:useBean id="onePerson" class="dev.demo.jsp.Person">
    <jsp:setProperty name="onePerson" property="name" value="zhang fei"></jsp:setProperty>
    <jsp:setProperty name="onePerson" property="age" value="18"></jsp:setProperty>
</jsp:useBean>

<p>Bean信息是：</p>
<jsp:getProperty name="onePerson" property="name"></jsp:getProperty>
<jsp:getProperty name="onePerson" property="age"></jsp:getProperty>


<hr/>
<h2>HTTP 头部请求实例</h2>
<table width="100%" border="1" align="center">
    <tr bgcolor="#949494">
        <th>Header Name</th>
        <th>Header Value(s)</th>
    </tr>
    <%
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String paramName = (String) headerNames.nextElement();
            out.print("<tr><td>" + paramName + "</td>\n");
            String paramValue = request.getHeader(paramName);
            out.println("<td> " + paramValue + "</td></tr>\n");
        }
    %>
</table>

<hr/>
<h2>自动刷新实例</h2>
<%
    // 设置每隔5秒自动刷新
    // response.setIntHeader("Refresh", 5);
    // 获取当前时间
    Calendar calendar = new GregorianCalendar();
    String am_pm;
    int hour = calendar.get(Calendar.HOUR);
    int minute = calendar.get(Calendar.MINUTE);
    int second = calendar.get(Calendar.SECOND);
    if (calendar.get(Calendar.AM_PM) == 0) {
        am_pm = "AM";
    } else {
        am_pm = "PM";
    }
    String CT = hour + ":" + minute + ":" + second + " " + am_pm;
    out.println("当前时间: " + CT + "\n");
%>

<hr/>
<h1>使用 GET 方法读取数据</h1>
<form action="index.jsp" method="GET">
    站点名: <input type="text" name="name">
    <br/>
    网址: <input type="text" name="url"/>
    <input type="submit" value="提交"/>
</form>

<ul>
    <li><p><b>站点名:</b>
        <%= request.getParameter("name")%>
    </p></li>
    <li><p><b>网址:</b>
        <%= request.getParameter("url")%>
    </p></li>
</ul>




在线人数：<%=session.getAttribute("userNumber") %><br/>
</body>
</html>
