# mvc 模式

## MVC 案例

- 没有业务层，直接由 Servlet 调用 DAO，所以也没有事务操作。所以可以在 DAO 中直接获取 Connection 对象
- 采取 MVC 设计模式
- 使用到的技术：
  * MVC 设计模式：JSP、Servlet，POJO
  * 数据库使用 MySQL
  * 连接数据库需要使用 C3P0 数据库连接池
  * JDBC 工具采用 DBUtils
  * 页面上的提示操作使用 jQuery
- 技术难点：
  * 多个请求如何使用一个 Servlet ？
  * 如何模糊查询 ？
  * 如何在创建 或 修改 的情况下，验证用户名是否已经被使用，并给出提示